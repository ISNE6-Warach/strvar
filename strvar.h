#ifndef STRVAR_H
#define STRVAR_H

#include <iostream>

using namespace std;

namespace strvarken
{
	class StringVar
	{
	public:
		StringVar(int size);
		StringVar();
		StringVar(const char a[]);

		StringVar(const StringVar& string_object);
		~StringVar();

		int length() const;
		void input_line(istream& ins);

		friend ostream& operator<<(ostream& outs, const StringVar& the_string);

	private:
		char *value;
		int max_length;
	};
}

#endif //STRVAR_H